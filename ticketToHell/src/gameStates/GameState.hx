package gameStates;

import flixel.FlxCamera;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxState;
import flixel.group.FlxGroup;
import flixel.tile.FlxTilemap;
import gameObjects.GlobalGameData.GB;
import gameObjects.tools.LifeIncrement;
import gameObjects.tools.MyHUD;
import gameObjects.tools.Positioner;
import gameObjects.players.MainCharacter;
import gameObjects.weapons.BearBoomerang;
import gameObjects.enemies.Zombie;
import gameObjects.traps.FloorTrap;
import gameObjects.traps.RoofTrap;
import openfl.Assets;

/**
 * ...
 * @author Simon
 */
class GameState extends FlxState
{
	var mMap:FlxTilemap;
	var mMainCharacter:MainCharacter;
	var mBearBoomerang:BearBoomerang;
	var zombies:FlxTypedGroup<Zombie>;
	var lifeIncrementers:FlxTypedGroup<LifeIncrement>;
	var hud:MyHUD;
	var floorTramps:FlxTypedGroup<FloorTrap>;
	var roofTramps:FlxTypedGroup<RoofTrap>;
	
	public function new() 
	{
		super();
	}
	
	override public function create():Void 
	{
		//Primer mapa
		mMap = new FlxTilemap();
		mMap.loadMapFromCSV(Assets.getText(AssetPaths.TerrenoTemporal2_Capa_de_Patrones_1__csv), 
			Assets.getBitmapData(AssetPaths.TerrenoTemporalTiles__png), 32, 32);
		
		//Segundo Mapa
		var fondoMap:FlxTilemap = new FlxTilemap();
		fondoMap.loadMapFromCSV(Assets.getText(AssetPaths.TerrenoTemporal2_Capa_de_Patrones_2__csv), 
			Assets.getBitmapData(AssetPaths.hellBackground__jpg), 32, 32, 0, 0);
		add(fondoMap);

		GB.positioner = new Positioner(mMap);
		
		loadZombies();
		
		add(mMap);
		
		mBearBoomerang = new BearBoomerang();
		add(mBearBoomerang);
		
		mMainCharacter = new MainCharacter(100, 100, mBearBoomerang);
		add(mMainCharacter);
		add(mMainCharacter.get_aHud());
		GB.player = mMainCharacter;
		floorTramps = new FlxTypedGroup();

		for (x in 0...mMap.widthInTiles) 
		{
			for (y in 0...mMap.heightInTiles)
			{
				if (mMap.getTile(x, y)== 1) 
				{
					var floorTrap:FloorTrap = new FloorTrap(x * 32, y * 32);
					mMap.setTile(x, y, 0);
					floorTramps.add(floorTrap);
					add(floorTrap);
				}
			}
		}
		roofTramps = new FlxTypedGroup();
		
		for (x in 0...mMap.widthInTiles) 
		{
			for (y in 0...mMap.heightInTiles)
			{
				if (mMap.getTile(x, y)== 2) 
				{
					var roofTrap:RoofTrap = new RoofTrap(x * 32, y * 32);
					mMap.setTile(x, y, 0);
					roofTramps.add(roofTrap);
					add(roofTrap);
				}
			}
		}
		FlxG.camera.follow(mMainCharacter, FlxCameraFollowStyle.PLATFORMER);
		FlxG.camera.setScrollBoundsRect(0, 500, mMap.width, 600);
		FlxG.worldBounds.set(0, 0, mMap.width, mMap.height);
		
		FlxG.camera.setScrollBoundsRect(0, 500, fondoMap.width, 600);
		FlxG.worldBounds.set(0, 0, fondoMap.width, fondoMap.height);
	}
	
	
	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
		
		FlxG.collide(mMap, mMainCharacter);
		FlxG.collide(mMap, zombies);
		FlxG.collide(mMap, lifeIncrementers);
		FlxG.collide(mMainCharacter, zombies);
		FlxG.collide(zombies, floorTramps);
		FlxG.collide(zombies, roofTramps);
		FlxG.overlap(mMainCharacter, mBearBoomerang, agarrarOsoBoomerang);
		FlxG.overlap(mBearBoomerang, zombies, zombiesVsBoomerang);
		FlxG.overlap(mBearBoomerang, mMap, boomerangVsMap);
		FlxG.overlap(mMainCharacter, zombies, protagonistaVsZomies);
		FlxG.overlap(mMainCharacter, lifeIncrementers, protagonistaVsLifeIncrementers);
	}
	
	private function agarrarOsoBoomerang(aProtagonista:MainCharacter, aOsoBoomerang:BearBoomerang) 
	{
		aOsoBoomerang.agarrar();
		
	}
	
	private function zombiesVsBoomerang(aOsoBoomerang:BearBoomerang, aZombie:Zombie)
	{
		aZombie.attemptKill();
		aOsoBoomerang.hit();
	}
	
	private function boomerangVsMap(aOsoBoomerang:BearBoomerang, aMap:FlxTilemap)
	{
		var xIndex:Int = Std.int( aOsoBoomerang.x / 32);
		var yIndex:Int = Std.int( aOsoBoomerang.y / 32);
		
		var typeA:Int = aMap.getTile(xIndex, yIndex);
		var typeB:Int = aMap.getTile(xIndex + 1, yIndex);
		if (typeA != 0 || typeB != 0)
		{
			aOsoBoomerang.hit();
		}
	}
	
	private function protagonistaVsZomies(aProtagonista:MainCharacter, aZombie:Zombie)
	{
		aZombie.attackPlayer();
		aProtagonista.figthZombie(aZombie);
	}
	
	private function protagonistaVsLifeIncrementers(aProtagonista:MainCharacter, aLifeIncrement:LifeIncrement)
	{
		aProtagonista.updateLife(aLifeIncrement.getValue());
		aLifeIncrement.kill();
	}
	
	private function loadZombies()
	{
		zombies = new FlxTypedGroup();
		lifeIncrementers = new FlxTypedGroup();
		for (i in 1...99) 
		{
			var posX:Int = i * 200;
			var lifeIncrement:LifeIncrement = new LifeIncrement(20);
			zombies.add(new Zombie(posX, GB.positioner.setContactPosition(posX, 50), this, lifeIncrement));
			lifeIncrementers.add(lifeIncrement);
		}
		add(zombies);
		add(lifeIncrementers);
	}
	
	override public function destroy():Void 
	{
		super.destroy();
		GB.clear();
	}
}