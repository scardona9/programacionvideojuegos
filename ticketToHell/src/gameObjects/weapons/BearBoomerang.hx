package gameObjects.weapons;
import flixel.FlxObject;
import flixel.FlxSprite;
import gameObjects.GlobalGameData.GB;
import openfl.Assets;

/**
 * ...
 * @author Simon
 */
class BearBoomerang extends FlxSprite
{
	var direction:Int = 0;
	
	public function new() 
	{
		super(0, 0);
		
		loadGraphic(Assets.getBitmapData(AssetPaths.boomerang__png), true, 32, 32);
		animation.add("ida", [0, 1, 2, 3, 4, 5, 6, 7], 30);
		animation.add("vuelta", [8, 9, 10, 11, 12, 13, 14, 14, 15], 30);
		kill();
	}
	
	public function myReset(X:Float, Y:Float, xVelocity:Float, xAcceleration:Float, mDirection:Int):Void 
	{
		if (!alive && !exists)
		{
			super.reset(X, Y);
			animation.play("ida");
			
			handleVelocidadYAceleracion(xVelocity, xAcceleration, mDirection);
			
			offset.y = 20;
			width = 30;
			height = 10;
		}
	}
	
	override public function update(elapsed:Float):Void 
	{	
		if (animation.curAnim != null && velocity != null && animation.curAnim.name == "ida" && verificarDarVuelta())
		{
			animation.play("vuelta");
		}
		if (animation.curAnim != null && velocity != null && animation.curAnim.name == "vuelta") 
		{
			var target:FlxSprite = GB.player;
			var deltaX:Float = (target.x + target.width * 0.5) - (x + width * 0.5);
			var deltaY:Float = (target.y + target.height * 0.5) - (y + height);
			var length:Float = Math.sqrt(deltaX * deltaX + deltaY * deltaY);
			deltaY /= length;
			deltaX /= length;
			
			velocity.x = deltaX * (50000/length);
			velocity.y = deltaY * (50000/length);
		}
		super.update(elapsed);
	}
	
	public function agarrado():Void
	{
		animation.stop();
	}
	
	public function verificarDarVuelta()
	{
		return velocity.x < 10 && velocity.x > -10;
	}
	
	public function handleVelocidadYAceleracion(xVelocity:Float, xAcceleration:Float, mDirection:Int) 
	{
		direction = mDirection;
		if (direction == FlxObject.RIGHT)
		{
			velocity.x = xVelocity;
			acceleration.x = -xAcceleration;
		}
		else if (direction == FlxObject.LEFT)
		{
			velocity.x = -xVelocity;
			acceleration.x = xAcceleration ;
		}
		else
		{
			velocity.x = 0;
			acceleration.x = 0;
		}
	}
	
	public function hit()
	{
		velocity.x = 0;
		animation.play("vuelta");
	}
	
	public function agarrar()
	{
		if (animation.curAnim.name == "vuelta")
		{
			kill();
		}
	}
}