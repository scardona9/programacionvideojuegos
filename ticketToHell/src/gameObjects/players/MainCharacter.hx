package gameObjects.players;

import AssetPaths;
import gameObjects.tools.MyHUD;
import gameStates.Death;
import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import gameObjects.weapons.BearBoomerang;
import gameObjects.enemies.Zombie;
import openfl.Assets;

/**
 * ...
 * @author Simon
 */
class MainCharacter extends FlxSprite
{
	static private inline var ACCELERATIONY:Float = 1000;
	var direction:Int = 0;
	var osoBoomerang:BearBoomerang;
	var life:Int = 200;
	var mHud:MyHUD;

	public function new(?X:Float=0, ?Y:Float=0, aOsoBoomerang:BearBoomerang) 
	{
		super(X, Y);
		
		mHud = new MyHUD(life);
		
		loadGraphic(Assets.getBitmapData(AssetPaths.protagonista__png), true, 32, 32);
		
		animation.add("run", [40, 41, 42, 43, 44, 45, 46, 47, 48, 49], 30);
		animation.add("stand", [30, 31, 32, 33, 34, 35, 36, 37, 38, 39], 30);
		animation.add("jump", [70, 71, 72, 73, 74, 75, 76, 77, 78, 79], 30);
		animation.add("fall", [20, 21, 22, 23, 24, 25, 26, 27, 28, 29], 30);
		animation.add("hit", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 30);
		animation.add("dead", [10, 11, 12, 13, 14, 15, 16, 17, 18, 19], 5, false);
		animation.add("throw", [90, 91, 92, 93, 94, 95, 96, 97, 98, 99], 30);
		animation.play("stand");
		
		drag.x = 500;
		offset.y = 20;
		width = 30;
		height = 10;
		maxVelocity.x = 400;
		acceleration.y = ACCELERATIONY;
		
		osoBoomerang = aOsoBoomerang;
	}
	
	override public function update(elapsed:Float):Void 
	{
		acceleration.x = 0;
		if (animation.curAnim.name == "dead")
		{
			if (animation.finished)
			{				
				super.kill();
				FlxG.switchState(new Death());
			}
		}
		else
		{
			if (FlxG.keys.pressed.LEFT)
			{
				direction = FlxObject.LEFT;
				animation.play("run");
				acceleration.x =-1000;
			}
			if (FlxG.keys.pressed.RIGHT)
			{
				direction = FlxObject.RIGHT;
				animation.play("run");
				acceleration.x =1000;
			}
			
			handleJump();
			
			if (direction==FlxObject.RIGHT)
			{
				flipX = false;
			}
			else
			{
				flipX = true;
			}
			
			if (FlxG.keys.pressed.S)
			{
				var boomerangVelocity:Float = 1000;
				var boomerangAcceleration:Float = 2000;
				
				animation.play("throw");
				osoBoomerang.myReset(x, y, boomerangVelocity, boomerangAcceleration, direction);
			}
			else if (FlxG.keys.pressed.A)
			{
				animation.play("hit");
			} 
			else if (isTouching(FlxObject.DOWN) && velocity.x == 0)
			{
				animation.play("stand");
			}
			
			if (!isTouching(FlxObject.DOWN))
			{
				if (velocity.y > 0)
				{
					animation.play("fall");
				}else {
					animation.play("jump");
				}
			}
		}
		
		super.update(elapsed);
		
	}
	
	function handleJump():Void
	{
		if (FlxG.keys.justPressed.SPACE && isTouching(FlxObject.DOWN))
		{
			velocity.y=-500;
		}else
		if (FlxG.keys.justPressed.SPACE && isTouching(FlxObject.LEFT))
		{
			velocity.x = 300;
			velocity.y=-500;
		}else
		if (FlxG.keys.justPressed.SPACE && isTouching(FlxObject.RIGHT))
		{
			velocity.x = -300;
			velocity.y=-500;
		}
	}
	
	public function figthZombie(aZombie:Zombie)
	{
		if (animation.curAnim.name == "hit")
		{
			aZombie.attemptKill();
		}
		else
		{
			if (aZombie.isAlife())
			{
				life -= 1;
				mHud.updateHealth(-1);
				if (life <= 0)
				{
					animation.play("dead");
					aZombie.continueWalking();
				}
			}
		}
	}
	
	public function updateLife(aValue:Int):Void
	{
		life += aValue;
		mHud.updateHealth(aValue);
	}
	
	public function get_aHud():MyHUD 
	{
		return mHud;
	}
	
	public function set_aHud(value:MyHUD):MyHUD 
	{
		return mHud = value;
	}
	
}