package gameObjects.enemies;

import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.FlxState;
import flixel.group.FlxSpriteGroup;
import gameObjects.tools.LifeBar;
import gameObjects.tools.LifeIncrement;
import gameStates.GameState;
import openfl.Assets;
import gameObjects.GlobalGameData.GB;

/**
 * ...
 * @author Zhian
 */
class Zombie extends FlxSprite
{
	static private inline var ACCELERATION:Float = 500;
	static private inline var VELOCITY:Float = -20;
	var positionY:Float;
	var lifeBar:LifeBar;
	var life:Int = 5;
	var mLifeIncrement:LifeIncrement;
	
	
	public function new(?X:Float=0, ?Y:Float=0, aGameState:gameStates.GameState, aLifeIncrement:LifeIncrement) 
	{
		super(X, Y);
		lifeBar = new LifeBar(X, Y, life, this);
		aGameState.add(lifeBar);
		mLifeIncrement = aLifeIncrement;
		
		loadGraphic(Assets.getBitmapData(AssetPaths.zombiesprite__png), true, 32, 32);
		
		animation.add("appear", [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 5, false);
		animation.add("die", [11, 12, 13, 14, 15, 16, 17, 18], 5, false);
		animation.add("walk", [22, 23, 24, 25, 26, 27, 28, 29, 30, 31], 15);
		animation.add("attack", [33, 34, 35, 36, 37, 38, 39], 15, false);
		animation.add("idle", [40, 41, 42, 43, 44, 45], 30);
		animation.add("disappear", [10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0], 5, false);
		
		animation.play("appear");
		
		offset.y = 20;
		width = 30;
		height = 10;		
		maxVelocity.set(25, 0);	
		velocity.x = VELOCITY;
	}
	
	override public function update(elapsed:Float):Void 
	{
		if (animation.curAnim.name == "walk")
		{
			velocity.x = VELOCITY;
			velocity.y = 0;
			if (isTouching(FlxObject.LEFT))
			{	
				animation.play('disappear');				
			}
			if (!isTouching(FlxObject.DOWN))
			{
				acceleration.y = ACCELERATION;
				velocity.y = 500;
			}
		}
		else if (animation.curAnim.name == "appear")
		{
			velocity.x = 0;
			if (animation.finished)
			{									
				animation.play("walk");
			}
		}
		else if (animation.curAnim.name == "disappear")
		{
			if (animation.finished)
			{				
				resetZombie();
			}
		}
		else if (animation.curAnim.name == 'die')
		{
			velocity.x = 0;
			velocity.y = 0;
			if (animation.finished)
			{
				mLifeIncrement.myReset(x, y);
				kill();
			}
		}
		else if (animation.curAnim.name == "attack")
		{
			if (animation.finished)
			{									
				animation.play("walk");
			}
		}
		
		super.update(elapsed);
	}
	
	private function resetZombie()
	{
		x = GB.player.x + 500;
		y = GB.positioner.setContactPosition(x, 50);
		animation.play("appear");
	}
	
	public function attackPlayer()
	{
		if (animation.curAnim.name != 'die')
		{
			animation.play("attack");
			velocity.x = 0;
			velocity.y = 0;
		}
	}
	
	public function attemptKill() 
	{
		life--;
		lifeBar.set_life(life);
		if (life <= 0)
		{
			animation.play('die');
		}
	}
	
	override public function kill()
	{
		lifeBar.kill();
		super.kill();
	}
	
	public function continueWalking() 
	{
		animation.play('walk');
	}
	
	public function isAlife():Bool
	{
		return animation.curAnim.name != 'die' && life > 0;
	}
}