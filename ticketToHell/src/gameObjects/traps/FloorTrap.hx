package gameObjects.traps;

import flixel.FlxG;
import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import openfl.Assets;

/**
 * ...
 * @author ...
 */
class FloorTrap extends FlxSprite
{
	var maxDistance:Int = 10000;
	public function new(?X:Float=0, ?Y:Float=0) 
	{
		super(X, Y);
		loadGraphic(Assets.getBitmapData(AssetPaths.TerrenoTemporalTiles__png), true, 32, 32);
		animation.add("deep", [560], 30);
		animation.add("top", [406], 30);
		animation.play("deep");
		immovable = true;
	}
	
	public function distanceWithCharacter(x:Float, y:Float):Void
	{
		var xDistance:Float = (x - this.x)*(x - this.x);
		var yDistance:Float = (y - this.y)*(y - this.y);
		
		if (maxDistance > xDistance+yDistance){
			this.set_visible(false);
		}
		else{
			this.set_visible(true);
		}
 	}
	
	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
		this.distanceWithCharacter(GlobalGameData.player.x, GlobalGameData.player.y);
	}
	
}