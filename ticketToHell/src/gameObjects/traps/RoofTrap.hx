package gameObjects.traps;

import flixel.FlxObject;
import flixel.FlxSprite;
import flixel.system.FlxAssets.FlxGraphicAsset;
import openfl.Assets;

/**
 * ...
 * @author ...
 */
class RoofTrap extends FlxSprite
{
	var maxDistance:Int = 75000;
	public function new(?X:Float=0, ?Y:Float=0) 
	{
		super(X, Y);
		loadGraphic(Assets.getBitmapData(AssetPaths.TerrenoTemporalTiles__png), true, 32, 32);
		animation.add("trap", [158], 30);
		animation.play("trap");
		
	}
	
	public function distanceWithCharacter(x:Float, y:Float):Void
	{
		var xDistance:Float = (x - this.x)*(x - this.x);
		var yDistance:Float = (y - this.y)*(y - this.y);
		
		if (maxDistance > xDistance+yDistance){
			acceleration.y = 1000;
		}
		
 	}
	
	override public function update(elapsed:Float):Void 
	{
		super.update(elapsed);
		this.distanceWithCharacter(GlobalGameData.player.x, GlobalGameData.player.y);
		
		if (isTouching(FlxObject.DOWN)){
			this.visible = false;
		}
		
	}
	
}