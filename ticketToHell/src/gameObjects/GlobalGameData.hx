package gameObjects;
import gameObjects.players.MainCharacter;
import gameObjects.tools.Positioner;

/**
 * ...
 * @author Simon
 */
typedef GB = GlobalGameData;
class GlobalGameData
{

	public static var player:MainCharacter;
	public static var positioner:Positioner;
	
	public function new() 
	{
		
	}
	
	public static function clear():Void
	{
		player = null;
		positioner = null;
	}
	
}