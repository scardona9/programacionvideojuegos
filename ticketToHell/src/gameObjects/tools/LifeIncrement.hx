package gameObjects.tools;
import flixel.FlxSprite;
import openfl.Assets;

/**
 * ...
 * @author Simon
 */
class LifeIncrement extends FlxSprite
{
	var mValue:Int;

	public function new(aValue:Int)
	{
		super(0, 0);
		mValue = aValue;
		
		loadGraphic(Assets.getBitmapData(AssetPaths.elementals__png), true, 32, 32);
		
		animation.add("stand", [3, 4, 5], 30);
		kill();
		offset.y = 20;
		width = 30;
		height = 10;
	}
	
	public function myReset(X:Float, Y:Float):Void 
	{
		if (!alive && !exists)
		{
			super.reset(X, Y);
			animation.play("stand");
			
			acceleration.y = 1000;
			offset.y = 20;
			width = 30;
			height = 10;
		}
	}
	
	public function getValue():Int
	{
		return mValue;
	}
	
}