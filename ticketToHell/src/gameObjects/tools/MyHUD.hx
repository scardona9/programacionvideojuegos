package gameObjects.tools;

import flixel.group.FlxSpriteGroup;
import flixel.text.FlxText;
import flixel.ui.FlxBar;

/**
 * ...
 * @author Simon
 */
class MyHUD extends FlxSpriteGroup
{
	private var healthDisplay:FlxText;
	
	private var hp:Int;
	private var maxHp:Int;
	
	public function new(aMaxHp:Int) 
	{
		super();
		scrollFactor.x = 0;
		scrollFactor.y = 0;
		
		healthDisplay = new FlxText(2, 2);
		hp = aMaxHp;
		maxHp = aMaxHp;
		add(healthDisplay);
	}
	
	override public function update(elapsed:Float) {
		healthDisplay.text = "Health: " + hp + "/" + maxHp;
		super.update(elapsed);
	}
	
	public function updateHealth(num:Int):Void {
		hp += num;
		if (hp > maxHp) {
			hp = maxHp;
		}
	}
	
}