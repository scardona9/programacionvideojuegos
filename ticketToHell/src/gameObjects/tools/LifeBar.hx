package gameObjects.tools;
import flixel.FlxSprite;
import flixel.group.FlxSpriteGroup;
import flixel.ui.FlxBar;
import flixel.util.FlxColor;

/**
 * ...
 * @author Simon
 */
class LifeBar extends FlxBar
{
	var life:Float;
	var mMaxLife:Float;
	var mBeing:FlxSprite;

	public function new(X:Float, Y:Float, aMaxLife:Float, aBeing:FlxSprite) 
	{
		super(X, Y, FlxBarFillDirection.LEFT_TO_RIGHT, 40, 5);
		createFilledBar(FlxColor.BLACK, FlxColor.GREEN, true);
		
		mMaxLife = aMaxLife;
		life = aMaxLife;
		mBeing = aBeing;
	}
	
	override public function update(elapsed:Float) {
		x = mBeing.x ;
		y = mBeing.y - 40;
		value = life;
		setRange(0, mMaxLife);
		super.update(elapsed);
	}
	
	public function get_life():Float
	{
		return life;
	}
	
	public function set_life(value:Float):Float 
	{
		return life = value;
	}
}