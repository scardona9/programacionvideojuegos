package gameObjects.tools;
import flixel.FlxObject;
import flixel.tile.FlxTilemap;

/**
 * ...
 * @author Zhian
 */
class Positioner extends FlxObject
{
	private var mMap:FlxTilemap;
	

	public function new(aMap:FlxTilemap) 
	{
		super();
		mMap = aMap;		
	}
	
	public function setContactPosition(x:Float, y:Float):Float
	{
		var xIndex:Int = Std.int( x / 32);
		var yIndex:Int = Std.int( y / 32);
		
		var type:Int = mMap.getTile(xIndex, yIndex);
				
		while (type == 0)
		{	
			yIndex = yIndex + 1;
			type = mMap.getTile(xIndex, yIndex);
		}
		
		return (yIndex - 1) * 32;
	}
	
}